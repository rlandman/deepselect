# deepselect

Recursively traverses the DOM (starting at a given node, document.body by default) and searches for all elements matching a given query selector. Any (open) shadow DOM boundary will be pierced.

Disclaimer:
Only elements for which the full selector matches within their own accessible DOM will match.
Elements for which the first part of the selector matches their hosting DOM and the last part matches their own (shadow) DOM (obviously) will not be found.

# examples

``` js
import deepselect from 'deepselect';
const matches = deepselect('div .someClass .someOtherClass');
const matchesArray = [...matches];
const firstMatch = matchesArray.length ? matchesArray[0] : undefined;
```

``` js
import deepselect from 'deepselect';
const matches = deepselect('div .someClass .someOtherClass', document.getElementById('#startNode'));
const matchesArray = [...matches];
const firstMatch = matchesArray.length ? matchesArray[0] : undefined;
```

# install

With [npm](https://npmjs.org) do:

```
npm install deepselect
```

# test

With [npm](https://npmjs.org) do:

```
npm test
```