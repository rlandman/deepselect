const deepselect = (selector, node = document.body, matches = new Set()) => {
  if (node) {
    [...node.querySelectorAll(selector)].forEach(match => matches.add(match));
    const shadowRoots = [node.shadowRoot, ...[...node.querySelectorAll('*')].map((e) => e.shadowRoot)];
    shadowRoots.filter((sr) => sr).forEach(nd =>
      deepselect(selector, nd, matches),
    );
  }

  return matches;
};

/**
 * Recursively traverses the DOM (starting at a given node, document.body by default) and searches
 * for all elements matching a given query selector. Any (open) shadow DOM boundary will be pierced.
 *
 * Disclaimer:
 * Only elements for which the full selector matches within their own accessible DOM will match.
 * Elements for which the first part of the selector matches their hosting DOM and the last part matches
 * their own (shadow) DOM will not be found.
 *
 * @param {string} selector - The CSS query selector to search with.
 * @param {Node} node - The parent DOM node to query in (optional: the default will be document.body).
* @throws {TypeError} - Thrown if incorrect parameters were supplied.
 * @returns {Set} A set containing all matching HTML elements.
 */
export default (selector, node = document.body) => {
  if (!selector || typeof selector !== 'string') {
    throw new TypeError('Function deepselect: missing or incorrect parameter(s)');
  }

  return deepselect(selector, node, new Set());
};