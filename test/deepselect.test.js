import { expect } from '@open-wc/testing';
import deepselect from 'deepselect';

function initialize() {
  document.body.innerHTML = '';

  const level0Div = document.createElement('div');
  level0Div.setAttribute('class', 'level0 light');
  const level1Div = document.createElement('div');
  level1Div.setAttribute('class', 'level1 light');
  const level2Div = document.createElement('div');
  level2Div.setAttribute('class', 'level2 shadow');
  const level3Div = document.createElement('div');
  level3Div.setAttribute('class', 'level3 shadow');
  const level4Div = document.createElement('div');
  level4Div.setAttribute('class', 'level4 shadow');

  level0Div.appendChild(level1Div);

  const level1DivShadowRoot = level1Div.attachShadow({ mode: 'open' });
  level1DivShadowRoot.appendChild(level2Div);

  const level2DivShadowRoot = level2Div.attachShadow({ mode: 'open' });
  level2DivShadowRoot.appendChild(level3Div);

  const level3DivShadowRoot = level3Div.attachShadow({ mode: 'open' });
  level3DivShadowRoot.appendChild(level4Div);

  document.body.appendChild(level0Div);
}

describe('deepselect', () => {
  const scenarios = [
    { description: 'No selector', selector: undefined, errorThrown: true },
    { description: 'Wrong selector', selector: 1, errorThrown: true },
    { description: 'Body selector', selector: 'body', matches: 0 },
    { description: 'Div selectors', selector: 'div', matches: 5 },
    { description: 'Div selector', selector: '.level1', matches: 1 },
    { description: 'Light divs', selector: '.light', matches: 2 },
    { description: 'Shadow divs', selector: '.shadow', matches: 3 },
  ];
  scenarios.forEach(scenario => {
    it(scenario.description, () => {
      try {
        initialize();

        const matches = deepselect(scenario.selector);
        if (scenario.errorThrown) {
          expect.fail('An error should have been thrown');
        } else {
          expect([...matches].length).to.equal(scenario.matches);
        }
      } catch (e) {
        if (!scenario.errorThrown) {
          expect.fail('An error should not have been thrown');
        } else {
          expect(e instanceof TypeError).to.be.true;
        }
      }
    });
  });
});
